#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"
. "${TESTDIR}/common/update-test-path"

#########
# Setup #
#########
trap "setup_failure" EXIT

ensure_dbus_session

setup_success

###########
# Execute #
###########

check_time() {
  NAME=$1
  shift
  MIN=$1
  shift
  MAX=$1
  shift
  START=$(($(date +%s%N)/1000000))
  OUTPUT_FILE=$(mktemp)
  $* > $OUTPUT_FILE 2>&1
  RET=$?
  END=$(($(date +%s%N)/1000000))
  MILLISECS=$(($END - $START))
  if [ $RET -ne 0 ] ; then
    result=failure
    cat $OUTPUT_FILE
  elif [ $MILLISECS -lt $MIN ] ; then
    result=failure
    cat $OUTPUT_FILE
  elif [ $MILLISECS -gt $MAX ] ; then
    result=failure
    cat $OUTPUT_FILE
  else
    result=success
  fi
  echo "${NAME}: command took ${MILLISECS}ms ($result), expected [${MIN}ms, ${MAX}ms]"
  rm -f $OUTPUT_FILE
  return 0
}

# Just send 5 messages without any load
test_basic() {
    set -e

    pkill dbus-test-tool || true
    sleep 1

    dbus-test-tool echo --name=test.basic &
    PID=$!
    /bin/sleep 0.5
    check_time test_basic 0 1000 dbus-test-tool spam --dest=test.basic --count=5
    RET=$?
    kill $PID
    wait $PID 2>/dev/null || /bin/true
    if [ $RET -ne 0 ] ; then
      return 1
    fi

    return 0
}

# Just send 5 messages with some load:
# - 1 spammer reusing the connection
# - 1 spammer using a new connection each time (and generating NameOwnerChanged)
test_some_load() {
    set -e

    pkill dbus-test-tool || true
    sleep 1

    dbus-test-tool echo --name=test.some.load.service &
    SERVICE_PID=$!
    dbus-test-tool echo --name=test.some.load.spam.service &
    SPAMMER_SERVICE_PID=$!
    /bin/sleep 0.5

    dbus-test-tool spam --dest=test.some.load.spam.service --count=1000000 --payload=spam >/dev/null 2>&1 &
    SPAMMER_CLIENT1_PID=$!
    dbus-test-tool spam --dest=test.some.load.spam.service --count=1000000 --payload=newspam --messages-per-conn=1 >/dev/null 2>&1 &
    SPAMMER_CLIENT2_PID=$!
    sleep 1

    check_time test_some_load 0 4000 dbus-test-tool spam --dest=test.some.load.service --count=5
    RET=$?
    for i in $SPAMMER_CLIENT1_PID $SPAMMER_CLIENT2_PID $SERVICE_PID $SPAMMER_SERVICE_PID ; do
        kill $i
        wait $i 2>/dev/null || /bin/true
    done
    if [ $RET -ne 0 ] ; then
      return 1
    fi

    return 0
}

# Similar to test_some_load but with 100 threads
test_heavy_load() {
    set -e

    pkill dbus-test-tool || true
    sleep 1

    dbus-test-tool echo --name=test.heavy.load.service &
    SERVICE_PID=$!
    dbus-test-tool echo --name=test.heavy.load.spam.service &
    SPAMMER_SERVICE_PID=$!
    /bin/sleep 0.5

    SPAMMER_CLIENT_PIDS=
    for i in `seq 1 50` ; do
        dbus-test-tool spam --dest=test.heavy.load.spam.service --count=1000000 --payload=spam >/dev/null 2>&1 &
        SPAMMER_CLIENT_PIDS="$SPAMMER_CLIENT_PIDS $!"
        dbus-test-tool spam --dest=test.heavy.load.spam.service --count=1000000 --payload=newspam --messages-per-conn=1 >/dev/null 2>&1 &
        SPAMMER_CLIENT_PIDS="$SPAMMER_CLIENT_PIDS $!"
    done
    sleep 1

    check_time test_heavy_load 0 8000 dbus-test-tool spam --dest=test.heavy.load.service --count=5
    RET=$?
    for i in $SPAMMER_CLIENT_PIDS $SERVICE_PID $SPAMMER_SERVICE_PID ; do
        kill $i
        wait $i 2>/dev/null || /bin/true
    done
    if [ $RET -ne 0 ] ; then
      return 1
    fi

    return 0
}

# Register plenty of various match rules to see if it slows dbus-daemon down
test_match_rules() {
    set -e

    pkill dbus-test-tool || true
    sleep 1

    BUS=session

    MONITOR_PIDS=

    # Do not use xargs: xargs does not kill its children on SIGTERM

    # On the system bus:
    # - max_match_rules_per_connection = 512
    # - max_connections_per_user = 256

    for i in `seq 1 20` ; do
        L=$(for n in `seq 1 500` ; do echo "type=method_call,interface=com.example,member=Spam$n" ; done)
        dbus-monitor --$BUS $L > /dev/null &
        MONITOR_PIDS="$MONITOR_PIDS $!"

        L=$(for n in `seq 1 500` ; do echo "interface=com.example,member=Spam$n" ; done)
        dbus-monitor --$BUS $L > /dev/null &
        MONITOR_PIDS="$MONITOR_PIDS $!"

        L=$(for n in `seq 1 500` ; do echo "type=method_call,member=Spam$n" ; done)
        dbus-monitor --$BUS $L > /dev/null &
        MONITOR_PIDS="$MONITOR_PIDS $!"

        L=$(for n in `seq 1 500` ; do echo "member=Spam$n" ; done)
        dbus-monitor --$BUS $L > /dev/null &
        MONITOR_PIDS="$MONITOR_PIDS $!"
    done

    dbus-test-tool echo --name=test.match.rules --$BUS &
    PID=$!
    /bin/sleep 0.5
    check_time test_match_rules 0 8000 dbus-test-tool spam --dest=test.match.rules --count=5 --$BUS
    RET=$?
    for i in $PID $MONITOR_PIDS ; do
        kill $i
        wait $i 2>/dev/null || /bin/true
    done
    if [ $RET -ne 0 ] ; then
      return 1
    fi

    return 0
}

trap "test_failure" EXIT

test_basic
test_some_load
test_heavy_load
test_match_rules

